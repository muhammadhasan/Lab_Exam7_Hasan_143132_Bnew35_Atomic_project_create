<?php
namespace App\SummaryOfOrganization;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class SummaryOfOrganization extends DB
{
    public $id;
    public $org_name;
    public $org_summary;

    public function __construct(){

        parent::__construct();
    }
    public function index(){
        echo "I'm inside the index of SummaryOfOrganization Class";
    }
    public function setData($data=NULL){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('org_name',$data)){
            $this->org_name=$data['org_name'];
        }
        if(array_key_exists('org_summary',$data)){
            $this->org_summary=$data['org_summary'];
        }
    }
    public function store(){
        $arrData = array($this->org_name,$this->org_summary);
        $sql="insert into summaryoforganization(org_name, org_summary) VALUES (?,?)";

        $STH= $this->DBH->prepare($sql); //create a object
        $result= $STH->execute($arrData);
        if($result)
            Message::setMessage("Success! Data has been inserted successfully!");
        else
            Message::setMessage("Fail! Data has not been inserted successfully");

        Utility::redirect('create.php');

    }
}