<?php
namespace App\Email;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class Email extends DB
{
    public $id;
    public $name;
    public $email_address;

    public function __construct(){

        parent::__construct();
    }
    public function index(){
        echo "I'm inside the index of Email Class";
    }
    public function setData($data=NULL){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }
        if(array_key_exists('email_address',$data)){
            $this->email_address=$data['email_address'];
        }
    }
    public function store(){
        $arrData = array($this->name,$this->email_address);
        $sql="insert into email_address(name, email_address) VALUES (?,?)";

        $STH= $this->DBH->prepare($sql); //create a object
        $result= $STH->execute($arrData);
        if($result)
            Message::setMessage("Success! Data has been inserted successfully!");
        else
            Message::setMessage("Fail! Data has not been inserted successfully");

        Utility::redirect('create.php');

    }

}
