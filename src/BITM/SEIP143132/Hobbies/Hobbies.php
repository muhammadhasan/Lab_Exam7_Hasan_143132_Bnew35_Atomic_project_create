<?php
namespace App\Hobbies;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;

class Hobbies extends DB
{
    public $id;
    public $name;
    public $hobbies;

    public function __construct(){

        parent::__construct();
    }
    public function index(){
        echo "I'm inside the index of Hobbies Class";
    }
    public function setData($data=NULL){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }
        if(array_key_exists('hobbies',$data)){
            $this->hobbies=$data['hobbies'];
        }
    }
    public function store(){
       // foreach($this->hobbies as $hobie) {
            $str = implode(',', $this->hobbies);
            echo $str;

           // die();

            $arrData = array($this->name,$str);
            $sql="insert into hobbies(name, hobbies) VALUES (?,?)";

            $STH= $this->DBH->prepare($sql); //create a object
            $result= $STH->execute($arrData);




        if($result)
            Message::setMessage("Success! Data has been inserted successfully!");
        else
            Message::setMessage("Fail! Data has not been inserted successfully");

        Utility::redirect('create.php');

    }




}
